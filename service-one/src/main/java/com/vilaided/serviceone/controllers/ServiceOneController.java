package com.vilaided.serviceone.controllers;

import com.vilaided.serviceone.model.ServiceOne;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/first")
public class ServiceOneController {

    @GetMapping
    public ServiceOne getServiceOne(){
        return new ServiceOne(1,"Hello from service 1");
    }
}
