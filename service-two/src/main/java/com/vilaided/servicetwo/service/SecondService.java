package com.vilaided.servicetwo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class SecondService {
    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private ServiceOneClient oneClient;

    public Object getByFeignClient(){
        return oneClient.getFromServiceOneFeign();
    }
    public Object getByRestTemplate(){return restTemplate.getForObject("http://first-service/first/",Object.class);}

}
