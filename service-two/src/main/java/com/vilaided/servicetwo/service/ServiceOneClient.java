package com.vilaided.servicetwo.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "first-service")
public interface ServiceOneClient {
    @GetMapping(value = "/first")
    public Object getFromServiceOneFeign();
}
