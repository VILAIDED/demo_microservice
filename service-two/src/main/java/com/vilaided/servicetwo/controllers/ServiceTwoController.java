package com.vilaided.servicetwo.controllers;

import com.vilaided.servicetwo.service.SecondService;
import com.vilaided.servicetwo.service.ServiceOneClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.ws.rs.core.Response;

@RestController
@RequestMapping("/second")
public class ServiceTwoController {
    @Autowired
    private SecondService service;

    @GetMapping(value = "/rest")
    public ResponseEntity getByRestemplate(){
        Object object = service.getByRestTemplate();
        return ResponseEntity.ok(object);
    }
    @GetMapping(value = "/feign")
    public ResponseEntity getByFeignClient(){
        return ResponseEntity.ok(service.getByFeignClient());
    }
}
